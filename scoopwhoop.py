import requests
from bs4 import BeautifulSoup
from collections import defaultdict
from selenium import webdriver


page_link = 'https://hindi.scoopwhoop.com/category/life/'
path = '/Users/manch/Downloads/chromedriver'

driver = webdriver.Chrome(path)
driver.implicitly_wait(30)
driver.get(page_link)

soup = BeautifulSoup(driver.page_source, 'lxml')
driver.quit()


posts = soup.find_all('div', class_='cardType3')

result = []

for post in posts:
    try:
        image = post.a.find_all('div', class_='ct3-left')[0].div.img['src']
        title = post.a.find_all('div', class_='ct3-right')[0].p.text
        result.append({'title': title, 'image': image})
    except Exception as e:
        pass

print(result)
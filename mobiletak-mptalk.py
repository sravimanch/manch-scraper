import requests
from bs4 import BeautifulSoup

page_link = 'https://www.mobiletak.in/mp-tak'

page_response = requests.get(page_link)

soup = BeautifulSoup(page_response.content, 'html.parser')

mobiltalk_listcard = soup.find_all('div', {'class': 'mobiletak-listcard'})[0]

print(mobiltalk_listcard)

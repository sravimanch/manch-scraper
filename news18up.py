import requests
from bs4 import BeautifulSoup
from collections import defaultdict
import sys

state = 'uttar-pradesh'

try:
    state = sys.argv[1]
except:
    pass

page_link = 'https://hindi.news18.com/{}/'.format(state)

page_response = requests.get(page_link)

soup = BeautifulSoup(page_response.content, 'html.parser')

posts = soup.find_all('div', class_='blog-list-blog')


result = []

print('processing {}......'.format(state))

for post in posts:
    try:
        title = post.p.a.text
        videoIcon = post.figure.find_all('a', {'class': 'vodeoiconb'})
        if (videoIcon):
            continue
        image = post.figure.find_all('a')[1].img['src']
        result.append({'title': title, 'image': image})
    except Exception as e:
        pass

print(result)

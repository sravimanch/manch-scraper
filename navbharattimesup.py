import requests
from bs4 import BeautifulSoup
from collections import defaultdict
import sys

page_link = 'https://navbharattimes.indiatimes.com/state/uttar-pradesh/articlelist/21236867.cms'


try:
    page_link = sys.argv[1]
except:
    pass

print('processing: ', page_link)
page_response = requests.get(page_link)

soup = BeautifulSoup(page_response.content, 'html.parser')

list_subsections = soup.find_all('div', class_='list_subsections')

result = defaultdict()

for subsections in list_subsections:
    city = subsections.h3.a.text
    result[city] = []
    d = []
    for li in subsections.ul:
        image = li.find_all('div', class_='imgsec')[0].a.img['src']
        title = li.find_all('div', class_='textsec')[0].a.text
        d.append({'image': image, 'title': title})

    result[city] = d


print(result)


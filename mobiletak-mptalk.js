const puppeteer = require('puppeteer');

const pageUrl = 'https://www.mobiletak.in/mp-tak';
//http://books.toscrape.com/

let scrape = async () => {
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    await page.goto(pageUrl);
    await page.waitFor(1000);
    await page.waitForSelector('.ng-lazyloaded');
    await page.waitForSelector('.mobiletak-listtitle');

    const result = await page.evaluate(() => {
        const image = document.querySelector('.ng-lazyloaded').getAttribute('src')
        console.log('card: ', image);
        const title = document.querySelector('.mobiletak-listtitle a').getAttribute('title');
        const href = document.querySelector('.mobiletak-listtitle a').getAttribute('href');
        console.log('title: ', title);
        return {
            title,
            image,
            href
        }
    });
    browser.close();
    return result;
}

scrape().then(value => {
    console.log(value);
})